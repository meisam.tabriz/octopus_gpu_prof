Step-by-step guide for profiling the GPU version of Octopus
-----------------------------------------------------------

We use NVIDIA Nsight Systems which is a low overhead performance analysis tool for profiling the octopus.
This guide refers to the Octopus 11 and NVIDIA Nsight systems 2021. The name of the executables, flags, and the default value of parameters may be different in other versions.

1.	Make sure that the octopus have been compiled with the CUDA support. It should be listed in the `Configuration options` section of the octopus output. For example as: \
`Configuration options  : maxdim3 openmp mpi `**`cuda`**` sse2 avx` \
Please refer to the [octopus installation guide]( https://octopus-code.org/new-site/develop/manual/install/) for detailed compilation instructions. \
Nsight systems only has built-in support for tracing the OpenMPI and MPICH implementations of MPI. If you are using a different MPI implementation, you can use [NVTX PMPI wrappers](https://github.com/NVIDIA/cuda-profiler/tree/master/nvtx_pmpi_wrappers) for profiling. For this guide we will use the OpenMPI implementation.
2.	Enable the time profiling in your octopus input file using the [ProfilingMode]( https://octopus-code.org/new-site/develop/variables/execution/optimization/profilingmode/) option. This is required for activating the NVTX markers and ranges. \
`ProfilingMode = prof_time`
3.	Install the Nsight Systems CLI. For collecting the profiling data, you need to have Nsight Systems CLI installed on your target machine. Please refer to the [NVIDIA Nsight Systems installation guide](https://docs.nvidia.com/nsight-systems/InstallationGuide/index.html) for more details. If you want to run Nsight Systems inside a container, please refer to [this guide]( https://developer.nvidia.com/blog/nvidia-nsight-systems-containers-cloud/). Many of the NVIDIA GPU Cloud catalog already include the Nsight Systems. \
Nsight systems is available on the MPCDF clusters as a module and can be loaded as: \
`module load cuda nsight_systems/2021` \
You should use the same version of CUDA toolkit as for your octopus compilation.
4.	[optional] By default, the Nsight systems will use `/tmp/nvidia` to store temporary files during data collection. In case you are running the profiler on a diskless machine (such as MPCDF’s Raven cluster) you must set the TMPDIR environment variable in your job submission script to another fast storage device instead. For example: `export TMPDIR=/ptmp/$USER/`
5.	Run the octopus code with profiler, tracing the CUDA, NVTX, and MPI. For example: (adapt this to your MPI launcher, number of the MPI tasks and the octopus path) \
**`nsys profile -t cuda,nvtx,mpi --mpi-impl=openmpi`**` srun -n 2 /usr/bin/octopus` \
The octopus will run as usual and at the end of the run a report file with `.qdrep` extension will be generated.
6.	Inspect the generated report file. The best way of inspecting the report file is using the Nsight Systems GUI which is included in your Nsight Systems installation. You can also use the MPCDF clusters (login or rvs nodes) for running the GUI. 
After running the `nsys-ui`, open the report file and you will see the timeline view of the report. \
![Sample Octopus profile in Nsight Systems GUI](https://gitlab.com/meisam.tabriz/octopus_gpu_prof/-/raw/main/nsys_octopus.png) \
You can expand the rows for NVTX to see the annotation of the code ranges. CUDA API row shows the relevant API calls. \
Check out the [Nsight Systems documentation](https://docs.nvidia.com/nsight-systems/UserGuide/index.html#report) for detailed information on using the Nsight Systems GUI.



